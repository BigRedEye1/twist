#include <twist/test/test.hpp>
#include <twist/test/assert.hpp>

#include <twist/stdlike/mutex.hpp>
#include <twist/stdlike/thread.hpp>

TEST_SUITE(TwistExamples) {

  // Report deadlock with fibers, hangs with threads
  TWIST_ITERATE_TEST(Deadlock, 3s) {
    twist::stdlike::mutex a;
    twist::stdlike::mutex b;

    twist::stdlike::thread t1([&]() {
      a.lock();
      b.lock();
      b.unlock();
      a.unlock();
    });

    twist::stdlike::thread t2([&]() {
      b.lock();
      a.lock();
      a.unlock();
      b.unlock();
    });

    t1.join();
    t2.join();
  }
}

RUN_ALL_TESTS()
