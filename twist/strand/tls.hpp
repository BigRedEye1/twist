#pragma once

#include <wheels/support/assert.hpp>

#include <array>
#include <atomic>
#include <functional>

namespace twist {
namespace strand {

//////////////////////////////////////////////////////////////////////

static const size_t kTLSSlots = 1024;

//////////////////////////////////////////////////////////////////////

using TLS = std::array<void*, kTLSSlots>;

//////////////////////////////////////////////////////////////////////

// Singleton
// Access via TLSManager::Instance()

class TLSManager {
 public:
  using Ctor = std::function<void*()>;
  using Dtor = std::function<void(void*)>;

 private:
  struct Slot {
    Ctor ctor;
    Dtor dtor;
  };

 public:
  static TLSManager& Instance() {
    static TLSManager instance;
    return instance;
  }

  size_t AcquireSlot(Ctor ctor, Dtor dtor) {
    size_t index = next_free_slot_.fetch_add(1);
    WHEELS_VERIFY(index < kTLSSlots, "TLS overcommit");
    slots_[index] = {ctor, dtor};
    return index;
  }

  void* Access(size_t slot_index) {
    return Access(slot_index, AccessTLS());
  }

  void* Access(size_t slot_index, TLS& tls) {
    if (tls[slot_index]) {
      return tls[slot_index];
    }
    tls[slot_index] = slots_[slot_index].ctor();
    return tls[slot_index];
  }

  void Destroy(TLS& tls) {
    size_t slots_used = next_free_slot_.load();

    for (size_t i = 0; i < slots_used; ++i) {
      if (tls[i] != nullptr) {
        slots_[i].dtor(tls[i]);
      }
    }
  }

 private:
  static TLS& AccessTLS();

 private:
  Slot slots_[kTLSSlots];
  std::atomic<size_t> next_free_slot_{0};
};

//////////////////////////////////////////////////////////////////////

class ManagedTLS {
 public:
  ManagedTLS() {
    tls_.fill(nullptr);
  }

  TLS& Access() {
    return tls_;
  }

  ~ManagedTLS() {
    TLSManager::Instance().Destroy(tls_);
  }

 private:
  TLS tls_;
};

}  // namespace strand
}  // namespace twist
