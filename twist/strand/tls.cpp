#include <twist/strand/tls.hpp>

#include <twist/fiber/runtime/scheduler.hpp>

namespace twist {
namespace strand {

// NB: destroyed before TLSManager
static thread_local ManagedTLS tls;

TLS& TLSManager::AccessTLS() {
  auto* fiber = fiber::TryGetCurrentFiber();
  if (fiber) {
    return fiber->AccessFLS();
  } else {
    return tls.Access();
  }
}

}  // namespace strand
}  // namespace twist
