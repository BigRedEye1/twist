#pragma once

#include <cstdlib>
#include <cstdint>

namespace twist {
namespace strand {

int FutexWait(uint32_t* addr, uint32_t expected);

void FutexWakeOne(uint32_t* addr);
void FutexWakeAll(uint32_t* addr);

}  // namespace strand
}  // namespace twist
