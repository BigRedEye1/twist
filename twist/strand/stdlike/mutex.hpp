#pragma once

#if defined(TWIST_FIBERS)

// cooperative user-space fibers

#include <twist/fiber/stdlike/mutex.hpp>

namespace twist::strand::stdlike {

using mutex = fiber::Mutex;  // NOLINT

}  // namespace twist::strand::stdlike

#else

// native threads

#include <mutex>

namespace twist::strand::stdlike {

using mutex = std::mutex;  // NOLINT

}  // namespace twist::strand::stdlike

#endif
