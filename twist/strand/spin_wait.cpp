#include <twist/strand/spin_wait.hpp>

#if defined(TWIST_FAULTY) || defined(TWIST_FIBERS) || PROC_COUNT == 1

#define SPIN_MULTI_CORE 0
#pragma message("SpinWait in single-core mode")

#else

#define SPIN_MULTI_CORE 1
#pragma message("SpinWait in multi-core mode")

#endif

namespace twist {
namespace strand {

void SpinWait::SpinOnce() noexcept {
#if (SPIN_MULTI_CORE)
  SpinOnceMultiCore();
#else
  SpinOnceSingleCore();
#endif
}

}  // namespace strand
}  // namespace twist
