#pragma once

#include <twist/strand/stdlike.hpp>

#include <wheels/support/cpu.hpp>

#include <thread>

namespace twist {
namespace strand {

/*
 * Use in spin-wait loops
 *
 * SpinWait spin_wait;
 * while (locked_.exchange(true)) {
 *   spin_wait();
 * }
 */

class SpinWait {
  // TODO: Remove magic constant
  // https://stackoverflow.com/a/25168942/13581739
  static const size_t kIterationsBeforeYield = 100;

 public:
  void operator()() noexcept {
    SpinOnce();
  }

  void SpinOnce() noexcept;

  void Reset() {
    iteration_count_ = 0;
  }

 private:
  // Implementation for threads backend
  void SpinOnceMultiCore() {
    iteration_count_++;
    if (iteration_count_ < kIterationsBeforeYield) {
      wheels::SpinLockPause();
    } else {
      std::this_thread::yield();
    }
  }

  // Implementation for fibers backend
  void SpinOnceSingleCore() {
    // Yield immediately
    twist::strand::stdlike::this_thread::yield();
  }

 private:
  size_t iteration_count_{0};
};

}  // namespace strand
}  // namespace twist
