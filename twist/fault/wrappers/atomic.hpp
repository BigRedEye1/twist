#pragma once

#include <twist/fault/adversary/inject_fault.hpp>

#include <twist/strand/stdlike/atomic.hpp>

#include <atomic>

namespace twist {
namespace fault {

// https://en.cppreference.com/w/cpp/atomic/atomic

template <typename T>
class FaultyAtomic {
 public:
  FaultyAtomic() {
    AccessAdversary();
  }

  explicit FaultyAtomic(T initial_value) : underlying_(initial_value) {
    AccessAdversary();
  }

  // NOLINTNEXTLINE
  T load(std::memory_order order = std::memory_order_seq_cst) const {
    InjectFault();
    T value = underlying_.load(order);
    InjectFault();
    return value;
  }

  operator T() const noexcept {
    return load();
  }

  // NOLINTNEXTLINE
  void store(T value, std::memory_order order = std::memory_order_seq_cst) {
    InjectFault();
    underlying_.store(value, order);
    InjectFault();
  }

  FaultyAtomic& operator=(T value) {
    store(value);
    return *this;
  }

  // NOLINTNEXTLINE
  T exchange(T new_value, std::memory_order order = std::memory_order_seq_cst) {
    InjectFault();
    const T prev_value = underlying_.exchange(new_value, order);
    InjectFault();
    return prev_value;
  }

  // NOLINTNEXTLINE
  bool compare_exchange_weak(T& expected, T desired, std::memory_order success,
                             std::memory_order failure) {
    InjectFault();
    bool succeeded =
        underlying_.compare_exchange_weak(expected, desired, success, failure);
    InjectFault();
    return succeeded;
  }

  // NOLINTNEXTLINE
  bool compare_exchange_weak(
      T& expected, T desired,
      std::memory_order order = std::memory_order_seq_cst) {
    return compare_exchange_weak(expected, desired, order, order);
  }

  // NOLINTNEXTLINE
  bool compare_exchange_strong(T& expected, T desired,
                               std::memory_order success,
                               std::memory_order failure) {
    InjectFault();
    bool succeeded = underlying_.compare_exchange_strong(expected, desired,
                                                         success, failure);
    InjectFault();
    return succeeded;
  }

  // NOLINTNEXTLINE
  bool compare_exchange_strong(
      T& expected, T desired,
      std::memory_order order = std::memory_order_seq_cst) noexcept {
    return compare_exchange_strong(expected, desired, order, order);
  }

  // NOLINTNEXTLINE
  T fetch_add(const T value,
              std::memory_order order = std::memory_order_seq_cst) {
    InjectFault();
    T prev_value = underlying_.fetch_add(value, order);
    InjectFault();
    return prev_value;
  }

  // NOLINTNEXTLINE
  T fetch_sub(const T value,
              std::memory_order order = std::memory_order_seq_cst) {
    InjectFault();
    T prev_value = underlying_.fetch_sub(value, order);
    InjectFault();
    return prev_value;
  }

  // Prefix increment & decrement

  T operator++() {
    return fetch_add(1) + 1;
  }

  T operator--() {
    return fetch_sub(1) - 1;
  }

  // Postfix increment & decrement

  T operator++(int) {
    return fetch_add(1);
  }

  T operator--(int) {
    return fetch_sub(1);
  }

  // Wait / notify

  // NOLINTNEXTLINE
  void wait(T old) {
    while (load() == old) {
      FutexWait(/*expected=*/old);
    }
  }

  // NOLINTNEXTLINE
  void notify_one() {
    FutexWakeOne();
  }

  // NOLINTNEXTLINE
  void notify_all() {
    FutexWakeAll();
  }

  // Direct access to futex syscall

  void FutexWait(T expected) {
    InjectFault();
    underlying_.FutexWait(expected);
    InjectFault();
  }

  void FutexWakeOne() {
    InjectFault();
    underlying_.FutexWakeOne();
    InjectFault();
  }

  void FutexWakeAll() {
    InjectFault();
    underlying_.FutexWakeAll();
    InjectFault();
  }

 private:
  strand::stdlike::atomic<T> underlying_;
};

}  // namespace fault
}  // namespace twist
