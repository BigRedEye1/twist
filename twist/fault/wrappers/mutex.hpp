#pragma once

#include <twist/strand/stdlike/mutex.hpp>
#include <twist/strand/stdlike/thread.hpp>

namespace twist {
namespace fault {

class FaultyMutex {
 public:
  FaultyMutex();

  // std::mutex-like / Lockable
  void lock();      // NOLINT
  bool try_lock();  // NOLINT
  void unlock();    // NOLINT

 private:
  twist::strand::stdlike::mutex impl_;
  twist::strand::stdlike::thread_id owner_id_;
};

}  // namespace fault
}  // namespace twist
