#pragma once

#include <twist/fault/random/integer.hpp>

#include <atomic>

namespace twist {
namespace fault {

class RandomEvent {
 public:
  RandomEvent(size_t freq) : freq_(freq) {
    Reset();
  }

  // Wait-free
  bool Test() {
    if (left_.fetch_sub(1) == 1) {
      // Last tick
      Reset();
      return true;
    }
    return false;
  }

  void Reset() {
    left_.exchange(RandomUInteger(1, freq_));
  }

 private:
  size_t freq_;
  std::atomic<int> left_{0};
};

}  // namespace fault
}  // namespace twist
