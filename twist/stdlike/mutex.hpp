#pragma once

#if defined(TWIST_FAULTY)

#include <twist/fault/wrappers/mutex.hpp>

namespace twist::stdlike {

using mutex = fault::FaultyMutex;  // NOLINT

}  // namespace twist::stdlike

#else

#include <mutex>

namespace twist::stdlike {

using ::std::mutex;

}  // namespace twist::stdlike

#endif
