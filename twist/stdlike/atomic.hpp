#pragma once

#if defined(TWIST_FAULTY)

#include <twist/fault/wrappers/atomic.hpp>

namespace twist::stdlike {

template <typename T>
using atomic = fault::FaultyAtomic<T>;  // NOLINT

}  // namespace twist::stdlike

#else

#include <twist/strand/stdlike/atomic.hpp>

#include <atomic>

namespace twist::stdlike {

// We need std::atomic<T>::wait
// using ::std::atomic;
using strand::stdlike::atomic;

}  // namespace twist::stdlike

#endif
