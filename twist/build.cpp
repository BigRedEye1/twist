
#if (TWIST_FIBERS) && !(TWIST_FAULTY)

#error \
    "Invalid twist build configuration: fibers execution backend without fault injection"

#endif

#if defined(TWIST_FIBERS)
#pragma message("Virtual threads backend: fibers")
#else
#pragma message("Virtual threads backend: threads")
#endif

#if defined(TWIST_FAULTY)
#pragma message("Fault injection enabled")
#endif
