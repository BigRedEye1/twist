#pragma once

#include <twist/test/chrono.hpp>

#include <wheels/support/time.hpp>

namespace twist::test::util {

using StopWatch = wheels::StopWatch<test::SteadyClock>;

}  // namespace twist::test::util
