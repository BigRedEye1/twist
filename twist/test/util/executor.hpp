#pragma once

#include <wheels/test/test_framework.hpp>
#include <twist/strand/stdlike.hpp>
#include <twist/test/util/affinity.hpp>

#include <functional>
#include <vector>

namespace twist::test::util {

using ThreadRoutine = std::function<void()>;

////////////////////////////////////////////////////////////////////////////////

namespace detail {
void RunThreadRoutine(ThreadRoutine routine);
}  // namespace detail

////////////////////////////////////////////////////////////////////////////////

class Executor {
 public:
  void Submit(ThreadRoutine routine);

  ~Executor() {
    Join();
  }

  void Join();

 private:
  std::vector<strand::stdlike::thread> threads_;
  bool joined_{false};
};

////////////////////////////////////////////////////////////////////////////////

strand::stdlike::thread RunThread(ThreadRoutine routine);

}  // namespace twist::test::util
