#pragma once

#include <twist/test/util/executor.hpp>

namespace twist::test::util {

class RaceLatch {
 public:
  void Wait() {
    std::unique_lock lock(mutex_);
    while (!released_) {
      released_cond_.wait(lock);
    }
  }

  void Release() {
    std::lock_guard guard(mutex_);
    released_ = true;
    released_cond_.notify_all();
  }

 private:
  bool released_{false};
  strand::stdlike::mutex mutex_;
  strand::stdlike::condition_variable released_cond_;
};

class Race {
 public:
  // TODO: remove
  Race(size_t /*threads*/) {
  }

  Race() {
  }

  void Add(ThreadRoutine routine) {
    executor_.Submit([this, routine]() {
      start_.Wait();
      routine();
    });
  }

  void Run() {
    // Release participants
    start_.Release();
    // Join participants
    executor_.Join();
  }

  void Start() {
    Run();
  }

 private:
  RaceLatch start_;
  Executor executor_;
};

}  // namespace twist::test::util
