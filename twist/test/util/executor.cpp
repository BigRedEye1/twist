#include <twist/test/util/executor.hpp>

#include <twist/fault/adversary/adversary.hpp>

namespace twist::test::util {

namespace detail {

void RunThreadRoutine(ThreadRoutine routine) {
#if defined(TWIST_FAULTY) && !defined(TWIST_FIBERS)
  SetTestThreadAffinity();
#endif

#if defined(TWIST_FAULTY)
  fault::GetAdversary()->Enter();
#endif

  try {
    routine();
  } catch (...) {
    wheels::test::FailTestByException();
  }

#if defined(TWIST_FAULTY)
  fault::GetAdversary()->Exit();
#endif
}

}  // namespace detail

void Executor::Submit(ThreadRoutine routine) {
  threads_.emplace_back([routine]() {
    detail::RunThreadRoutine(routine);
  });
}

void Executor::Join() {
  if (joined_) {
    return;
  }
  for (auto& t : threads_) {
    t.join();
  }
  threads_.clear();
  joined_ = true;
}

////////////////////////////////////////////////////////////////////////////////

strand::stdlike::thread RunThread(ThreadRoutine routine) {
  return strand::stdlike::thread{[routine]() {
    detail::RunThreadRoutine(routine);
  }};
}

}  // namespace twist::test::util
