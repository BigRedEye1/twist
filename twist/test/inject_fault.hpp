#pragma once

namespace twist::test {

// Inject voluntary fault
void InjectFault();

bool FaultsEnabled();

}  // namespace twist::test
