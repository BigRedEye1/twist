#pragma once

#include <wheels/test/test_framework.hpp>

#define TWIST_ASSERT(cond, error_message) ASSERT_TRUE_M(cond, error_message)
