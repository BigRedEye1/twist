#include <twist/fiber/stdlike/thread.hpp>

#include <twist/fiber/sync/event.hpp>

namespace twist {
namespace fiber {

class ThreadLike::Impl {
 public:
  Impl(FiberRoutine routine) : routine_(std::move(routine)) {
    Spawn([this]() {
      Run();
    });
  }

  ~Impl() {
    if (!joined_) {
      std::terminate();
    }
  }

  void Join() {
    if (joined_) {
      return;  // already
    }
    done_.Await();
    joined_ = true;
  }

 private:
  void Run() {
    routine_();
    done_.Signal();
  }

 private:
  FiberRoutine routine_;
  bool joined_{false};
  OneShotEvent done_{"thread join"};
};

ThreadLike::ThreadLike(FiberRoutine routine)
    : pimpl_(std::make_unique<ThreadLike::Impl>(std::move(routine))) {
}

ThreadLike::ThreadLike(ThreadLike&& that) : pimpl_(std::move(that.pimpl_)) {
}

ThreadLike& ThreadLike::operator=(ThreadLike&& that) {
  pimpl_ = std::move(that.pimpl_);
  return *this;
}

ThreadLike::~ThreadLike() {
}

void ThreadLike::Join() {
  pimpl_->Join();
}

}  // namespace fiber
}  // namespace twist
