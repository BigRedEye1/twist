#include <twist/fiber/sync/futex.hpp>

#include <twist/fiber/runtime/wait_queue.hpp>

#include <wheels/support/singleton.hpp>

#include <map>

namespace twist {
namespace fiber {

using Address = uint32_t*;

class Futex {
 public:
  int Wait(Address address, uint32_t value) {
    if (*address == value) {
      WaitQueueFor(address).Park();
      return 0;
    }
    return -1;  // ~ EAGAIN
  }

  void WakeOne(Address address) {
    WaitQueueFor(address).WakeOne();
  }

  void WakeAll(Address address) {
    WaitQueueFor(address).WakeAll();
  }

 private:
  WaitQueue& WaitQueueFor(Address address) {
    auto [it, _] = queues_.try_emplace(address, "futex");
    return it->second;
  }

 private:
  std::map<Address, WaitQueue> queues_;
};

int FutexWait(Address address, uint32_t value) {
  return Singleton<Futex>()->Wait(address, value);
}

void FutexWakeOne(Address address) {
  return Singleton<Futex>()->WakeOne(address);
}

void FutexWakeAll(Address address) {
  return Singleton<Futex>()->WakeAll(address);
}

}  // namespace fiber
}  // namespace twist
