#pragma once

#include <cstdlib>
#include <cstdint>

namespace twist {
namespace fiber {

// https://en.wikipedia.org/wiki/Futex

// Returns 0 if the caller was woken up, -1 otherwise (EAGAIN)
int FutexWait(uint32_t* addr, uint32_t expected);

void FutexWakeOne(uint32_t* addr);
void FutexWakeAll(uint32_t* addr);

}  // namespace fiber
}  // namespace twist
