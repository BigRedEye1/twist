#pragma once

#include <memory>
#include <string>

namespace twist {
namespace fiber {

class WaitQueue {
 public:
  WaitQueue(const std::string& descr = "?");
  ~WaitQueue();

  void Park();

  void WakeOne();
  void WakeAll();

  bool IsEmpty() const;

 private:
  class Impl;
  std::unique_ptr<Impl> pimpl_;
};

}  // namespace fiber
}  // namespace twist
