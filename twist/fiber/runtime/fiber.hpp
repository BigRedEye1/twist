#pragma once

#include <context/context.hpp>
#include <context/stack.hpp>

#include <twist/fiber/runtime/api.hpp>

// TODO: ???
#include <twist/strand/tls.hpp>

#include <wheels/support/intrusive_list.hpp>

#include <string>

namespace twist {
namespace fiber {

//////////////////////////////////////////////////////////////////////

enum class FiberState {
  Starting,  // Initial state
  Runnable,  // In Scheduler run queue
  Running,
  Suspended,  // In wait queue or Scheduler sleep queue
  Terminated,
  Deadlocked,
};

class Fiber : public wheels::IntrusiveListNode<Fiber> {
  friend class Scheduler;

 public:
  FiberId Id() const {
    return id_;
  }

  context::ExecutionContext& Context() {
    return context_;
  }

  context::Stack& Stack() {
    return stack_;
  }

  FiberState State() const {
    return state_;
  }

  void SetState(FiberState target) {
    state_ = target;
  }

  std::string_view Where() const {
    return where_;
  }

  void SetWhere(std::string_view descr) {
    where_ = descr;
  }

  strand::TLS& AccessFLS() {
    return fls_.Access();
  }

  void InvokeUserRoutine() {
    routine_();
  }

  size_t StepCount() const {
    return steps_;
  }

 private:
  Fiber(FiberRoutine&& routine, context::Stack&& stack, FiberId id);

  static void Trampoline();

 private:
  FiberRoutine routine_;
  context::Stack stack_;
  context::ExecutionContext context_;
  FiberState state_;
  strand::ManagedTLS fls_;
  FiberId id_;
  std::string_view where_{"-"};
  size_t steps_{0};
};

}  // namespace fiber
}  // namespace twist
