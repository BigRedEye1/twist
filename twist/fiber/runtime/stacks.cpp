#include <twist/fiber/runtime/stacks.hpp>

#include <vector>

namespace twist {
namespace fiber {

using context::Stack;

//////////////////////////////////////////////////////////////////////

static size_t BytesToPagesRoundUp(size_t bytes) {
  static const size_t kPageSize = 4096;

  size_t pages = bytes / kPageSize;
  if (bytes % kPageSize != 0) {
    ++pages;
  }
  return pages;
}

//////////////////////////////////////////////////////////////////////

class StackAllocator {
  using Stack = context::Stack;

 public:
  StackAllocator();

  Stack Allocate();
  void Release(Stack stack);

  void SetMinStackSize(size_t bytes);

 private:
  static size_t DefaultStackSize() {
    return 64_KiB;
  }

 private:
  std::vector<Stack> pool_;
  size_t stack_size_pages_;
};

StackAllocator::StackAllocator()
    : stack_size_pages_(BytesToPagesRoundUp(DefaultStackSize())) {
}

Stack StackAllocator::Allocate() {
  if (!pool_.empty()) {
    Stack stack = std::move(pool_.back());
    pool_.pop_back();
    return stack;
  }
  return Stack::AllocatePages(stack_size_pages_);
}

void StackAllocator::Release(Stack stack) {
  if (stack.Size() == stack_size_pages_) {
    pool_.push_back(std::move(stack));
  }
}

void StackAllocator::SetMinStackSize(size_t bytes) {
  size_t pages = BytesToPagesRoundUp(bytes);
  if (pages > stack_size_pages_) {
    pool_.clear();
  }
  stack_size_pages_ = pages;
}

//////////////////////////////////////////////////////////////////////

static thread_local StackAllocator allocator;

Stack AllocateStack() {
  return allocator.Allocate();
}

void ReleaseStack(Stack stack) {
  allocator.Release(std::move(stack));
}

void SetMinStackSize(size_t bytes) {
  allocator.SetMinStackSize(bytes);
}

}  // namespace fiber
}  // namespace twist
