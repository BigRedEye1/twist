#include <twist/fiber/runtime/scheduler.hpp>
#include <twist/fiber/runtime/stacks.hpp>
#include <twist/fiber/report/stacktrace.hpp>

#include <wheels/support/exchange.hpp>
#include <wheels/support/compiler.hpp>
#include <wheels/support/panic.hpp>
#include <wheels/support/terminal.hpp>

#if defined(TWIST_FAULTY)
#include <twist/fault/random/helpers.hpp>
#endif

#include <thread>
#include <utility>
#include <iostream>
#include <sstream>

namespace twist {
namespace fiber {

//////////////////////////////////////////////////////////////////////

static thread_local Scheduler* current_scheduler;

Scheduler* GetCurrentScheduler() {
  WHEELS_VERIFY(current_scheduler, "Not in fiber context");
  return current_scheduler;
}

struct SchedulerScope {
  SchedulerScope(Scheduler* scheduler) {
    WHEELS_VERIFY(!current_scheduler,
                  "Cannot run scheduler from another scheduler");
    current_scheduler = scheduler;
  }

  ~SchedulerScope() {
    current_scheduler = nullptr;
  }
};

Fiber* GetCurrentFiber() {
  return GetCurrentScheduler()->GetCurrentFiber();
}

Fiber* TryGetCurrentFiber() {
  if (current_scheduler != nullptr) {
    return current_scheduler->GetCurrentFiber();
  } else {
    return nullptr;
  }
}

//////////////////////////////////////////////////////////////////////

Scheduler::Scheduler(size_t seed) : twister_(seed) {
}

// Operations invoked by running fibers

void Scheduler::SwitchToFiber(Fiber* fiber) {
  loop_context_.SwitchTo(fiber->Context());
}

void Scheduler::SwitchToScheduler(Fiber* me) {
  me->Context().SwitchTo(loop_context_);
}

// ~ System calls

void Scheduler::Spawn(FiberRoutine routine) {
  auto* created = CreateFiber(std::move(routine));
  Schedule(created);
}

void Scheduler::Yield() {
  if (preempt_disabled_) {
    return;
  }
  Fiber* caller = GetCurrentFiber();
  caller->SetState(FiberState::Runnable);
  SwitchToScheduler(caller);
}

void Scheduler::SleepFor(wheels::Duration duration) {
  Fiber* caller = GetCurrentFiber();

  bool first = sleep_queue_.IsEmpty();
  sleep_queue_.Put(caller, duration);
  if (first) {
    Spawn([this]() {
      TimeKeeper();
    });
  }
  caller->SetState(FiberState::Suspended);
  caller->SetWhere("sleep queue");
  SwitchToScheduler(caller);
}

void Scheduler::Suspend(std::string_view where) {
  Fiber* caller = GetCurrentFiber();
  caller->SetState(FiberState::Suspended);
  caller->SetWhere(where);

  SwitchToScheduler(caller);

  if (WHEELS_UNLIKELY(caller->State() == FiberState::Deadlocked)) {
    ReportFromDeadlockedFiber(caller);
    SwitchToScheduler(caller);
  }
}

void Scheduler::Resume(Fiber* fiber) {
  WHEELS_ASSERT(fiber->State() == FiberState::Suspended,
                "Unexpected fiber state");
  fiber->SetState(FiberState::Runnable);
  fiber->SetWhere("-");
  Schedule(fiber);
}

void Scheduler::Terminate() {
  Fiber* caller = GetCurrentFiber();
  caller->SetState(FiberState::Terminated);
  SwitchToScheduler(caller);
}

Fiber* Scheduler::GetCurrentFiber() const {
  return running_;
}

// Scheduling

void Scheduler::Run(FiberRoutine init) {
  SchedulerScope scope(this);
  Spawn(std::move(init));
  RunLoop();
  CheckDeadlock();
}

void Scheduler::Tick() {
  time_.AdvanceBy(std::chrono::microseconds(10));
}

void Scheduler::RunLoop() {
  while (!run_queue_.IsEmpty()) {
    Tick();
    Fiber* next = PickReadyFiber();
    Step(next);
    Reschedule(next);
  }
}

void Scheduler::PollSleepQueue() {
  auto now = SleepQueue::Clock::now();
  while (Fiber* f = sleep_queue_.Poll(now)) {
    Resume(f);
  }
}

bool Scheduler::IsIdle() const {
  return run_queue_.IsEmpty() && !sleep_queue_.IsEmpty();
}

void Scheduler::TimeKeeper() {
  while (!sleep_queue_.IsEmpty()) {
    PollSleepQueue();

    if (IsIdle()) {
      time_.AdvanceTo(sleep_queue_.NextDeadLine());
      continue;
    } else if (!sleep_queue_.IsEmpty()) {
      Yield();  // Reschedule poller
    } else {
      break;
    }
  }
}

Fiber* Scheduler::PickReadyFiber() {
#if defined(TWIST_FAULTY)
  return fault::UnlinkRandomItem(run_queue_);
#else
  return run_queue_.PopFront();
#endif
}

void Scheduler::Step(Fiber* fiber) {
  ++switch_count_;
  fiber->SetState(FiberState::Running);
  ++fiber->steps_;
  running_ = fiber;
  SwitchToFiber(fiber);
  running_ = nullptr;
}

void Scheduler::Reschedule(Fiber* fiber) {
  switch (fiber->State()) {
    case FiberState::Runnable:  // From Yield
      Schedule(fiber);
      break;
    case FiberState::Suspended:  // From Suspend
      // Do nothing
      break;
    case FiberState::Terminated:  // From Terminate
      Destroy(fiber);
      break;
    default:
      WHEELS_PANIC("Unexpected fiber state");
      break;
  }
}

void Scheduler::Schedule(Fiber* fiber) {
  run_queue_.PushBack(fiber);
}

Fiber* Scheduler::CreateFiber(FiberRoutine routine) {
  auto stack = AllocateStack();
  FiberId id = ids_.NextId();

  Fiber* fiber = new Fiber(std::move(routine), std::move(stack), id);

  alive_.insert(fiber);
  return fiber;
}

void Scheduler::Destroy(Fiber* fiber) {
  ReleaseStack(std::move(fiber->Stack()));
  alive_.erase(fiber);
  delete fiber;
}

// O(1)
size_t Scheduler::AliveCount() const {
  return alive_.size();
}

uint64_t Scheduler::GenerateRandomNumber() {
  return twister_();
}

void Scheduler::CheckDeadlock() {
  if (run_queue_.IsEmpty() && AliveCount() > 0) {
    // Validate
    for (auto* f : alive_) {
      WHEELS_VERIFY(f->State() == FiberState::Suspended, "Internal error");
    }
    ReportDeadlockAndDie();
  }
}

void Scheduler::ReportDeadlockAndDie() {
  std::cout << wheels::terminal::Red()                       // Do not format!
            << "DEADLOCK detected in fiber scheduler: "      //
            << alive_.size() << " fibers blocked"            //
            << wheels::terminal::Reset() << std::endl        //
            << "Steps made: " << switch_count_ << std::endl  //
            << std::endl;

  for (auto* fiber : alive_) {
    fiber->SetState(FiberState::Deadlocked);
    SwitchToFiber(fiber);
  }

  std::cout << "REPORT END" << std::endl;
  WHEELS_PANIC("Deadlock detected in fiber scheduler");
}

void Scheduler::ReportFromDeadlockedFiber(Fiber* me) {
  std::cout << wheels::terminal::Red()                 // Do not format!
            << "FIBER #" << me->Id()                   //
            << wheels::terminal::Reset()               //
            << " (blocked at: " << me->Where() << ")"  //
            << std::endl;

  // Stack trace
  std::ostringstream trace_out;
  PrintStackTrace(trace_out);
  const auto trace = trace_out.str();
  if (!trace.empty()) {
    std::cout << std::endl << trace << std::endl;
  }
}

}  // namespace fiber
}  // namespace twist
